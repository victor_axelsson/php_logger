<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

include_once 'logger.php'; 



if(isset($_GET['msg'])){
	$logger = new Logger(); 
	//$logger->systemLog($_GET['msg']); 
	$logger->debugLog($_GET['msg']); 
}

function printData($data){
	foreach ($data as $log) {
		echo "<tr>";
		echo "<td>".$log['msg']."</td>"; 
		echo "<td>".$log['type']."</td>"; 
		echo "<td>".$log['__timestamp']."</td>"; 
		echo "</tr>";
	}
}

function printSystemLogs(){
	$logger = new Logger(); 
	printData($logger->getLogsByType("system_log")); 
}

function printDebugLogs(){
	$logger = new Logger(); 
	printData($logger->getLogsByType("debug_log")); 
}
?>


<html>
<head>
	<title>Logger</title>

	<style>
		table{
			width: 50%;
			border: 1px solid black;
			float: left;
		}

		thead tr td{
			background-color: #000;
			color: #FFF;
		}

		tr:nth-child(even) {
		    background-color: #919DFF;
		}

		tr:nth-child(odd) {
		    background-color: #D4D8FF;
		}

	</style>
</head>
<body>
	
	<h1>System logs</h1>
	<table>
	<thead>
	  <tr>
	    <td>Message</td>
	    <td>Type</td>
	    <td>Timestamp</td>
	  </tr>
	</thead>
	<tbody>
		<?php printSystemLogs(); ?>
	</tbody>
	</table>

	<table>
	<thead>
	  <tr>
	    <td>Message</td>
	    <td>Type</td>
	    <td>Timestamp</td>
	  </tr>
	</thead>
	<tbody>
		<?php printDebugLogs(); ?>
	</tbody>
	</table>

</body>
</html> 