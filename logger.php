<?php

class Logger extends SQLite3{

	//Default ctor for the class
	public function __construct(){
	 	$this->open('logg.db');
	 	$ret = $this->query($this->getCreateLogTable());
	}

	private function getCreateLogTable(){
		$sql = 	"create table if not exists log ( ";
		$sql .= "id integer primary key autoincrement, ";
		$sql .= "msg text not null, ";
		$sql .= "type text not null, ";
		$sql .=	"__timestamp timestamp default CURRENT_TIMESTAMP )";
		return $sql; 
	}
	
	public function systemLog($msg){
		$stmt = $this->prepare("insert into log (msg, type) values (:msg, :type)");
		$stmt->bindValue(':msg', $msg, SQLITE3_TEXT);
		$stmt->bindValue(':type', "system_log", SQLITE3_TEXT);
		return $stmt->execute();
	}

	public function debugLog($msg){
		$stmt = $this->prepare("insert into log (msg, type) values (:msg, :type)");
		$stmt->bindValue(':msg', $msg, SQLITE3_TEXT);
		$stmt->bindValue(':type', "debug_log", SQLITE3_TEXT);
		return $stmt->execute();
	}	

	public function getLogs($limit = 100){
		$stmt = $this->prepare("select * from log order by __timestamp desc limit :limit ");
		$stmt->bindValue(':limit', $limit, SQLITE3_INTEGER);
		$result = $stmt->execute();

		return $this->formatData($result); 
	}

	private function formatData($data){
		$data = array(); 
		while($row = $result->fetchArray()){
			array_push($data, $row); 
		}
		return $data; 
	}

	public function getLogsByType($type, $limit = 100){
		$stmt = $this->prepare("select * from log where type = :type  order by __timestamp desc limit :limit ");
		$stmt->bindValue(':limit', $limit, SQLITE3_INTEGER);
		$stmt->bindValue(':type', $type, SQLITE3_TEXT);
		$result = $stmt->execute();

		return $this->formatData($result); 
	}

}


?>